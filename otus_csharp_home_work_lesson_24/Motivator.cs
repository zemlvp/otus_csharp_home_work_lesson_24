﻿using System;
using System.Drawing;

namespace otus_csharp_home_work_lesson_24
{
    /// <summary>
    /// Класс "мотиватора" - представляет из себя картинку в рамке и сообщение внизу
    /// </summary>
    public class Motivator
    {
        private Bitmap _bitmap;
        private Graphics _graphics;
        private readonly MotivatorSettings _motivatorSettings;
        private readonly string _imagePath;
        private readonly string _message;
        public const int MOTIVATOR_WIDTH = 600;
        public const int MOTIVATOR_HEIGHT = 800;
        /// <summary>
        /// Максимальная высота рамки для сообщения
        /// </summary>
        public const int MAX_HEIGHT_SIZE_MESSAGE = 70;  
        public MotivatorValidator Validator { get; set; }

        public Motivator(MotivatorSettings motivatorSettings, string imagePath, string message, MotivatorValidator validator)
        {
            _motivatorSettings = motivatorSettings;
            _imagePath = imagePath;
            _message = message;
            Validator = validator;
        }

        public void CreateMotivator()
        {
            _bitmap = new Bitmap(MOTIVATOR_WIDTH, MOTIVATOR_HEIGHT);
            _graphics = Graphics.FromImage(_bitmap);
            _graphics.Clear(_motivatorSettings.BackgroundColor);
        }

        public void AddImage(int x, int y, int width, int height)
        {
            if (Validator.IsValidSizeImage(x, y, width, height))
            {
                // рамка для картинки заданной толщины 
                Pen borderPen = new Pen(new SolidBrush(_motivatorSettings.BorderImageColor), _motivatorSettings.BorderImageWidth);
                //Rectangle rectImage = new Rectangle(20, 20, 560, 650);
                Rectangle rectImage = new Rectangle(x, y, width, height);
                _graphics.DrawRectangle(borderPen, rectImage);

                // добавим картинку в рамку 
                Image img = Image.FromFile(_imagePath);
                _graphics.FillRectangle(new TextureBrush(new Bitmap(img)), rectImage);
            }
            else
                throw new Exception($"Неправильные размеры для картинки");
        }

        public void AddMessage(int x, int y, int width, int height)
        {
            if (Validator.IsValidSizeMessage(x, y, width, height))
            {
                // рамка для сообщения
                //Rectangle rectMessage = new Rectangle(40, 700, 520, 70);
                Rectangle rectMessage = new Rectangle(x, y, width, height);

                // сообщение выровнить по центру
                StringFormat alignCenter = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                // шрифт сообщения
                Font fontMessage = new Font("Arial", _motivatorSettings.MessageSize, FontStyle.Bold, GraphicsUnit.Point);
                _graphics.DrawString(_message, fontMessage, _motivatorSettings.MessageColor, rectMessage, alignCenter);

                // сообщение помещаем в рамку для текста
                Pen borderRectangleMessage = new Pen(new SolidBrush(_motivatorSettings.BackgroundColor), 1);
                _graphics.DrawRectangle(borderRectangleMessage, rectMessage);
            }
            else
                throw new Exception($"Неправильный размер рамки для сообщения");
        }

        public string SaveImage()
        {
            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}{Guid.NewGuid()}.jpg";
            _bitmap.Save(filePath);

            return filePath;
        }
    }
}