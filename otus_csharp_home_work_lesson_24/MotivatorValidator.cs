﻿namespace otus_csharp_home_work_lesson_24
{
    public class MotivatorValidator
    {
        public bool IsValidSizeImage(int x, int y, int width, int height)
        {
            if (x + width > Motivator.MOTIVATOR_WIDTH ||
                y + height > Motivator.MOTIVATOR_HEIGHT - Motivator.MAX_HEIGHT_SIZE_MESSAGE)
                return false;

            return true;
        }

        public bool IsValidSizeMessage(int x, int y, int width, int height)
        {
            if (x + width > Motivator.MOTIVATOR_WIDTH ||
                y + height > Motivator.MOTIVATOR_HEIGHT ||
                height > Motivator.MAX_HEIGHT_SIZE_MESSAGE)
                return false;

            return true;
        }
    }
}