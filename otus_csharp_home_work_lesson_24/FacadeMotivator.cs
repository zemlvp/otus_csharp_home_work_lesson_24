﻿namespace otus_csharp_home_work_lesson_24
{
    public class FacadeMotivator
    {
        public string CreateMotivator(string imagePath, string message, MotivatorSettings motivatorSettings, MotivatorValidator validator)
        {            
            var motivator = new Motivator(motivatorSettings,
                                          imagePath, 
                                          message,
                                          validator);           

            motivator.CreateMotivator(); // предопределенный размер: ширина 600, высота 800
            motivator.AddImage(20, 20, 560, 650);
            motivator.AddMessage(40, 700, 520, 70);
            var pathMotivator =  motivator.SaveImage();

            return pathMotivator;   
        }
    }
}
