﻿using System;
using System.Diagnostics;
using System.Drawing;

namespace otus_csharp_home_work_lesson_24
{
    class Program
    {
        private static string appPath = AppDomain.CurrentDomain.BaseDirectory;

        static void Main()
        {
            // Две картинки для теста хранятся в папке проекта Images
            // Для тестирования в начале запускаем для GetTestData1, потом для GetTestData2
            
            var testData = GetTestData1();
            //var testData = GetTestData2();

            FacadeMotivator motivator = new FacadeMotivator();
            var motivatorImagePath = motivator.CreateMotivator(testData.imagePath, 
                                                               testData.message, 
                                                               testData.motivatorSettings,
                                                               new MotivatorValidator());

            ProcessStartInfo procInfo = new ProcessStartInfo
            {
                FileName = "mspaint.exe",
                Arguments = motivatorImagePath
            };
            Process.Start(procInfo);          
        }

        private static (MotivatorSettings motivatorSettings, string imagePath, string message) GetTestData1()
        {
            var motivatorSettings = new MotivatorSettings
            {
                BackgroundColor = ColorTranslator.FromHtml("#4a86de"),
                BorderImageColor = Color.White,
                BorderImageWidth = 12,
                MessageColor = Brushes.Yellow,
                MessageSize = 20
            };

            var imagePath = $"{appPath}\\Images\\Зима.jpg";
            var message = "Зима! Зима! Зима! Зима!Зима! Зима! Зима! Зима!Зима! Зима! Зима! Зима!";

           return (motivatorSettings, imagePath, message);            
        }

        private static (MotivatorSettings motivatorSettings, string imagePath, string message) GetTestData2()
        {
            var motivatorSettings = new MotivatorSettings
            {
                BackgroundColor = Color.Orange,
                BorderImageColor = Color.Red,
                BorderImageWidth = 20,
                MessageColor = Brushes.White,
                MessageSize = 24
            };

            var imagePath = $"{appPath}\\Images\\Лето.jpg";
            var message = "Лето! Лето! Лето! Лето!";

            return (motivatorSettings, imagePath, message);
        }
    }
}