﻿using System.Drawing;

namespace otus_csharp_home_work_lesson_24
{
    /// <summary>
    /// Настройки для мотиватора
    /// </summary>
    public class MotivatorSettings
    {
        /// <summary>
        /// Цвет фона
        /// </summary>
        public Color BackgroundColor;

        // Цвет рамки вокруг картинки
        public Color BorderImageColor;

        /// <summary>
        /// Толщина рамки вокруг картинки
        /// </summary>
        public int BorderImageWidth;

        /// <summary>
        /// Цвет сообщения
        /// </summary>
        public Brush MessageColor;

        /// <summary>
        /// Размер шрифта для сообщения
        /// </summary>
        public int MessageSize;
    }
}
